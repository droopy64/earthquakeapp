package com.it_system.earthquakeapp.Util;

import java.util.Random;

public final class RandomIntGenerator {

    public RandomIntGenerator() {
    }

    public static int generateRandomInt(int min, int max) {
        return new Random().nextInt(max - min) + min;
    }
}
